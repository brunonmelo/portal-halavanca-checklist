# Portal Halavanca BJJ Checklist

## checkpoints a serem entregues:
 - Layout base;
 - Tela de home;
 - Tela de sobre;
 - Tela de contato;
 - Tela de Termos de uso | Politicas de privacidade;
 - Área gerencial;
 - Funcionalidade de duplo idioma;
 - Funcionalidade do Blog;
 - Funcionalidade da área logada;
 - Funcionalidade da loja;
 - Funcionalidade do EAD;

### Layout base
  - [x] implementação dos estilos bases
    - [x] fonte;
    - [x] cores;
    - [x] espaçamentos;
    - [x] icones;
  - [x] implementação do menu
    - [x] desenho do layout;
    - [x] criação da animação do mouse over;
    - [x] criação na navegação no click;
    - [x] desenho do layout para a versão mobile;
    - [x] customização do side menu na versão mobile;
  - [x] implementação do footer
    - [x] desenho do layout;
    - [x] desenho da versão mobile;
    - [x] implementação dos links sociais;
    - [x] inclusão de links para tela de termos de aceite e privacidade;
  - [x] montagem do titulos dos tópicos;
  - [x] desenho da animação dos botões;
  - [x] desenho das sobras personalizadas das sessões;
  - [x] desenho dos itens dos forms;
  - [x] montagem da infraestruta para o server side renderer;
  - [x] hospedagem dos arquivos e configuração dos serviços e servidores no Firebase;

### Tela de home
  - [x] setor do carrosel
    - [x] desenho do layout;
    - [x] desenho do layout na versão mobile;
    - [x] desenho dos botões para mudança dos slides;
    - [x] implementação do comportamento de auto slide;
    - [x] implementação do comportamento para exibir os componentes dinamicamente:
      - [x] botão de ação para área interna do site;
      - [x] botão de ação para link externo ao site;
      - [x] texto descritivo;
    - [x] integração para carregar os dados de forma dinâmica da base de dados
  - [x] setor nossa equipe
    - [x] desenho do layout;
      - [x] imagem do cabeçalho;
      - [x] colunas das sessões;
    - [x] desenho do layout na versão mobile;
    - [x] integração para carregar os dados de forma dinâmica da base de dados
  - [x] setor do vídeo
    - [x] desenho do layout;
    - [x] integração com os videos "embedded";
    - [x] desenho da versão mobile;
    - [x] integração para carregar os dados de forma dinâmica da base de dados

### Tela de sobre
  - [x] criação do componente das sessões "sobre";
    - [x] desenho do layout;
    - [x] desenho do layout na versão mobile;
  - [x] implementação de comportamento para renderização dos componentes de forma dinâmica;
  - [x] integração para carregar os dados de forma dinâmica da base de dados

### Tela de contato
  - [x] Criação do formulário para contato;
  - [x] desenho do layout;
  - [x] desenho do layout na versão mobile;
  - [x] integração com o reCaptcha do Google;
  - [x] validação de segurança dos dados do reCaptcha;
  - [x] implementação para salvar as mensagens em uma base de dados;

### Tela de Termos de uso | Politicas de privacidade
  - [x] desenho do layout;
  - [x] desenho do layout na versão mobile;
  - [x] integração para carregar os dados de forma dinâmica da base de dados;

### Área gerencial
#### Tela de Login
  - [x] desenho do layout;
  - [x] Criação do formulário de autenticação;
  - [x] integração com o reCaptcha do Google;
  - [x] validação de segurança dos dados do reCaptcha;
  - [x] integração com sistema de usuários do Firebase;
  - [x] validação se o usuário é admin;
  - [x] criação do comportamente que redireciona o usuário para home caso ele não seja admin;
  - [x] criação do botão para recuperação da senha;
  - [x] criação do card de versionamento;
  - [x] implementando lógica que previne o usuário acessar a página de autenticado sem estar autenticado;
  
#### Dashboad
  - [x] Desenho do layout;
  - [x] Criação do menu lateral com as subsessões da área gerencial;
  - [x] Criando cards com atalhos rápidos para as principais sessões da área gerencial;
  - [x] Criação do botão de logout;
  - [x] Criação de um banner informando a quantidade de mensagens não lidas;

#### Gerenciamento das páginas
  - [x] Criação dos cards para edição dos conteúdos das principais páginas;
  - [x] Adicionando botão de voltar no botton bar;
  - [x] Adicionado toolbar de navegação entre os setores da Home Page;

  - [x] Edição do carousel:
    - [x] Criando um componente para listagem das páginas do carousel;
    - [x] Adicionando botões para criação, edição e remoção dos itens do carousel;
    - [x] Criação de formulário para edição/criação de um item do carousel;
    - [x] Criação de item do formulário para adicionar imagem;
    - [x] Criando lógica para remover imagem;
    - [x] Montendo lógica para salvar item do carousel na base de dados;
    - [x] integração para carregar os dados de forma dinâmica da base de dados;
    - [x] Montango lógica para atualizar item já existente do carousel;
    - [x] Criando lógica para deletar item do carousel;

  - [x] Edição do sobre da Home:
    - [x] Criação de lista personalizada com os itens presentes no Sobre da Home;
    - [x] Adicionando botão para edição dos itens do Sobre da Home;
    - [x] Criando formulário para edição do Sobre da Home;
    - [x] Montando form para edição da imagem do Sobre da Home;
    - [x] Montendo lógica para salvar item do Sobre da Home na base de dados;

  - [x] Edição do Home Vídeo:
    - [x] Criando Form para edição do conteúdo do Home Vídeo;
    - [x] Adicionando botão para edição dos itens do Home Vídeo;
    - [x] Adicionando validação do conteúdo do "iframe" do vídeo;
    - [x] Adicionando preview do vídeo;
    - [x] Montendo lógica para salvar dados do Home Vídeo na base de dados;

#### Gerenciamento do Blog
  - [x] Criação dos cards para gerenciamento dos itens do blog;
  - [x] Novo Post;
    - [x] Criação do formulário para criação do Post do blog;
    - [x] Criando formatação especial para o blog, "negrito", "sublinhado", "italico";
    - [x] Criando botão para exibição do preview;
    - [x] Adicionando chave para seleção do idioma do post;
    - [x] Carregando lista de categorias e tags da base de dados;
    - [x] montando lógica para exibição das categorias de acordo com o idioma selecionado;
    - [x] Criação de form especial para seleção de multiplas categorias e tags;
    - [x] Criação de botão para criação de categoria dentro do seletor de categorias/tags;
    - [x] Adicionando lógica para limpar as categorias selecionadas caso o idioma for modificado;
    - [x] Adicionando lógica para salvar na base de dados as categorias/tags criadas;
    - [x] Adicionando forma para gerenciamento das imagens no post;
    - [x] Adicionando lógica para salvar posts na base de dados;
    - [x] montando separação lógica do conteudo dos posts pra otimizar os carregamentos;
  
  - [x] Gerenciamento de posts;
    - [x] integração para carregar os dados dos post de forma dinâmica da base de dados;
    - [x] criando listagem dos post criados;
    - [x] adicionando botão para encaminhar para tela de criação de novo post;
    - [x] adicionando botão para edição dos posts já existentes;
    - [x] criando lógica para atualização dos posts editados na base de dados;
    - [x] adicionando botão para deletar post;
    - [x] montando lógica para remover os posts da base de dados;
    - [x] Adicionando botão para carregar mais posts caso tenha muitos posts;
    - [x] montando lógica de carregamento dos posts que não foram carregados ainda, na sequência;
    - [x] montano lógia para ocultar o botão de carregar mais posts caso não tenha mais posts para serem carregados;

  - [x] Gerenciamento das tags e categorias;
    - [x] Criando listagem das categorias e tags;
    - [x] montando lógica para carregar os dados dinamicamente da base de dados;
    - [x] criando botão para adicionar nova categoria/tag de acordo com a lista selecionada;
    - [x] montando lógica para salvar nova categoria/tag na base de dados;
    - [x] adicionando botão para remover item selecionado;
    - [x] montando lógica para deletar item da base de dados;

#### Gerenciamento das mensagens
  - [x] Criando lista para listagem das mensagens;
  - [x] montando lógica para carregar as mensagens da base de dados;
  - [x] criando contador para mensagens não lidas;
  - [x] desenhando tela para exibição das mensagens e dados da mensagens;
  - [x] criando lógica para marcar a mensagem como lida depois de um tempo selecionada;
  - [x] adicionando estilo para diferênciar as mensagens já lidas na lista;
  - [x] Adicionando botão para carregar mais mensagens caso tenha muitas mensagens;
  - [x] adicionando lógica para ocultar o botão caso não tenha mais mensagens e adicionando uma mensagem de que não tem mais mensagens;

#### Gerenciamento dos usuários
  - [x] Criação de cards para gerenciamento dos usuários;
    - [x] Gerenciamento de carteirinhas:
      - [x] montando a lógica para carregar os dados da carteirinha na base de dados;
      - [x] Criação de formulários para edição das informações da carteirinha;
      - [x] Criação de formulário para adicionar os documentos nescessários;
      - [x] Criação de formulário para editar o valor da emissão das carteirinhas;
      - [x] Criação de lógica para salvar os dados na base de dados;
      - [x] Criação de lógica para deletar os dados na base de dados;
      - [x] Criação de lógica para atualizar os dados na base de dados;
    
    - [x] Gerenciamento dos certificados:
      - [x] criação de lista para exibição dos certificados elegiveis para serem submetidos;
      - [x] criação de lógica para carrega os certificados da base de dados;
      - [x] inclusão do botão para adicionar um novo certificado;
      - [x] montagem de formulário para os dados do certificado;
      - [x] criação de lógica para salvar novo formulário da base de dados;
      - [x] adicionando botão para edição dos certificados;
      - [x] adicionando lógica para atualizar os certificados modificados;
      - [x] adicionar botão para remoção do certificado selecionado;
      - [x] criando lógica para remoção do certificado da base de dados;

    - [x] Gerenciamento do módulo de professor:
      - [x] Criando formulários para os documentos requeridos para submeter o usuários como professor;
      - [x] adicionando lógica para carregar os dados da base de dados;
      - [x] adicionando botão salvar as modificações na base de dados;

    - [x] Gerenciamento da academia:
      - [x] Criação da lista para exibição dos tipos de academia;
      - [x] Criação de lógica para carregar os tipos de academia da base de dados;
      - [x] Criação de botão para criação de um novo tipo de academia;
      - [x] criação de formulário para edição dos tipos de academia;
      - [x] criação de lógica para salvar novo tipo de academia na base de dados;
      - [x] criação de botão para editar tipo de academia selecionado;
      - [x] criação de lógica para atualização das modificações na base de dados;
      - [x] criação de botão para remover um tipo de academia;
      - [x] criação de lógica para remover tipo de academia da base de dados;

#### Gerenciamento do EAD 
  - [ ] criação de lista para exibir os cursos;
  - [ ] carregar os dados dos cursos na base de dados;
  - [ ] adicionando formulário para adicionar um novo curso;
  - [ ] Criação de cards para gerenciamento dos usuários;
  - [ ] criação de lógica para salvar novo curso na base de dados;
  - [ ] criação de lógica para atualizar novo curso na base de dados;
  - [ ] realizar descobertas de terefas adicionas quando a tela tiver sido desenhada;

#### Gerenciamento da Loja
  - [ ] realizar descoberta para saber se a ferramenta de loja utilizada terá um módulo de gerenciamento ingrado;

### Funcionalidade de duplo idioma
  - [x] desenho do layout do botão para mudança de idioma;
  - [x] criação de lógia para alternar entre os dois idiomas;
  - [x] criação de função para inclusão do layout dos pontos que podem ter dois idiomas selecionaveis;
  - [x] criação de função que modifica o formato da data de acordo com o idioma selecionado;
  - [x] montagem da lógica para identificar o idioma do navegador e já carregar no idioma correto;
  - [x] armazenamento nas configurações do navegador do último idioma selecionado;

### Funcionalidade do Blog
  - [x] desenho do layout da listagem dos posts;
  - [x] desenho do layout do menu lateral;
  - [x] desenho dos layouts na versão mobile;
  - [x] implementação da lógica para carregar os posts e categorias de acordo com o idioma selecionado da base de dados;
  - [x] implementação da lógica para carregar os dados de tag da base de dados;
  - [x] implementação da lógica para recarregar os dados de posts e categoria sempre que o idioma é modificado;
  - [x] implementação de filtro sempre que um tageamento o categoria for selecionado;
  - [x] desenho do layout quando um post for selecionado;
  - [x] desenho do layout do post na versão mobile;
  - [x] lógica para carregar os dados complementares quando um post for aberto;
  - [x] lógica para carregar os dados complesto quando a página for aberta diretamente em um post;
  - [x] criação de botões para compartilhamento dos posts das redes sociais;
  - [x] criação da lógica com botão para carregar mais posts da base de dados;
  - [x] desenho do layout com placehold para quando não houver nenhum post para ser listado;
  - [x] implementando campo de texto especial para os formatar os texto de acordo com a formatação;

### Funcionalidade da área logada
  - [x] criação de botão extra no menu para autenticação;
  - [x] implementação de função que atualiza nome do usuário no botão de autenticação após autenticação;
  - [x] salvando dados de sessão do usuário;
  - [x] implementação expiração de sessão após 1 hora;
  - [x] implementando lógica para deslogar o usuário após expiração da sessão;
  - [x] implementando lógica para buscar os dados do usuário na base de dados quando a sessão estiver ativa;
  - [x] implementando lógica que previne o usuário acessar a página de autenticado sem estar autenticado;
  - [x] adicionando atalhos para as telas de termos de aceite;

  - [x] tela de autenticação:
    - [x] desenho do layout da tela de autenticação;
    - [x] desenho do layout na versão mobile;
    - [x] criação do formulário de autenticação;
    - [x] incluindo lógica para realizar autenticação no sistema de autenticação do Firebase;
    - [x] incluindo validação de reCaptcha na retaguarda;
    - [x] incluindo botão para recuperação de senha;
    - [x] incluindo botão para criação de nova conta;

  - [x] tela de recuperação de senha:
    - [x] desenho do layout da tela de recuperação de senha;
    - [x] desenho do layout da versão mobile;
    - [x] validação do layout pra email;
    - [x] implementação da lógica para envio de email para recuperação de senha;
    - [x] incluindo validação de reCaptcha na retaguarda;

  - [x] tela de criação de conta:
    - [x] desenho do layout da tela de criação de conta;
    - [x] desenho do layout da versão mobile;
    - [x] implementação do formulário para criação de conta
    - [x] adicionando validações de senha no formulário;
    - [x] incluindo validação de reCaptcha na retaguarda;
    - [x] criando lógica para criação do usuário na base de autenticação do firebase;
    - [x] criando lógica para salvar os dados iniciais do usuário na base de dados;

  - [x] tela de dashboard da área logada:
    - [x] desenho do layout da tela de dashboard;
    - [x] desenho do layout da versão mobile;
    - [x] carregando os dados de usuário no painel do dashboard;
    - [x] incluindo lógica para carregar as informações do painel de acordo com a disponibilidade;
    - [x] criando lógica para modificação da cor do status de acordo com o status;
    - [x] adicionando botão para ajuda;
  
  - [x] menu de navegação:
    - [x] desenho do layout do menu de navegação;
    - [x] desenho do layout da versão mobile;
    - [x] criação da lógica para reajustar os botões de acordo com a quantidade exibida;
    - [x] criando os estilos e comportamento para cada tipo de botão;

  - [x] tela de meu perfil:
    - [x] implementação para carregar os dados do usuário da base de dados;
    - [x] criação de um form especial para realizar upload de fotos e arquivos;
    - [x] criação da lógica para fazer o upload do arquivo, na base de arquivos;
    - [x] criação de formulário para edição dos dados do usuário;
    - [ ] criação de lógica para apresentar dados no formulário de acordo com o tipo de usuário;
    - [ ] modelar a base de dados para salvar as referências cruzadas dos usuários;
    - [x] criação de função para atualizar os dados do usuário na base de dados;
    - [x] criação de função para criptografar os dados do usuário na base de dados;
    - [x] criação de função para descriptografar os dados do usuário na base de dados ao carregar os dados;
    - [x] criação de um form para mudança de senha;
    - [x] adicionar validação de senha para atualização das senhas;

  - [x] tela de carteirinha:
    - [x] desenhar o layout da tela de carteirinhas;
    - [x] carregar as informações das carteirinhas da base de dados;
    - [x] adicionar seletor do tipo de carteirinha e carregar as informações de acordo;
    - [x] adicionar os forms dos documentos nescessários;
    - [x] incluir a sessão dos dados pendentes e alertas;
    - [x] incluir opção de cancelamento da submissão;
    - [x] incluir informações das carteirinhas já cadastradas;
    - [x] incluir lembrete para atualizar o pagamento quando o prazo da crendêncial estiver perto de vencer;
    - [x] modelar a estrutura de dados para as informações das carteirinhas;
    
  - [ ] tela de gerenciamento dos alunos | professores | usuários:
    - [ ] desenhar o layout da tela de usuários;
    - [ ] carregar as informações dos usuários com base nas permissões;
    - [ ] desenhar a tela informação extra dos usuário;
    - [ ] desenhar os componentes para aprovação das requisições;
    - [ ] carregar da base de dados os dados dos usuários;
    - [ ] modelar na base de dados as informações de aprovação das requisições;
  
  - [ ] tela de certificados:
    - [ ] desenhar o layout da tela de certificados;
    - [ ] carregar as informações das carteirinhas da base de dados;
    - [ ] adicionar seletor do tipo de carteirinha e carregar as informações de acordo;
    - [x] adicionar os forms dos documentos nescessários;
    - [ ] incluir a sessão dos dados pendentes e alertas;
    - [ ] incluir informações dos certificados já cadastradas;
    - [ ] modelar a estrutura de dados para as informações de certificados;
  
  - [ ] tela de pagamento:
    - [ ] desenhar o layout da tela de certificados;
    - [ ] carregar as informações dos pagamentos pendentes da base de dados;
    - [ ] realizar descoberta de uma ferramenta de pagamento;
    - [ ] realizar implementação de uma ferramenta de pagamento;
    - [ ] realizar implementação para atualizar os dados na base de dados, de acordo com o que está sendo pago;

### Funcionalidade da loja
  - [ ] realizar descoberta da ferramenta de loja que será implementada;
  - [ ] criação do layout da loja de acordo com a possibilidade de customização da ferramenta;
  - [ ] desenhar o layout da loja;
  - [ ] realizar descoberta de como funciona o inventário de produtos;
  - [ ] realizar descoberta de como funciona o pagamento;
  - [ ] realizar descoberta de como funciona o sistema de gerenciamento das entregas;
  - [ ] realizar descobertas de terefas adicionas quando a tela tiver sido desenhada;

### Funcionalidade do EAD
  - [ ] tela para adquirir novos cursos:
    - [ ] criação do layout da sessão do EAD;
    - [ ] desenhar o layout a sessão do EAD;
    - [ ] carregar os dados dos cursos da base de dados;
    - [ ] realizar descobertas de terefas adicionas quando a tela tiver sido desenhada;

  - [ ] tela de pagamento dos cursos:
    - [ ] criação do layout da sessão do EAD;
    - [ ] desenhar o layout a sessão do EAD;
    - [ ] realizar descobertas de terefas adicionas quando a tela tiver sido desenhada;
    - [ ] realizar modelagem da base de dados para salvas os cursos adquidos nas informações do usuário;

  - [ ] tela para visualizar os cursos adquiridos:
    - [ ] criação do layout da sessão do EAD;
    - [ ] desenhar o layout a sessão do EAD;
    - [ ] realizar descoberta de implementação do Vimeo;
    - [ ] realizar descobertas de terefas adicionas quando a tela tiver sido desenhada;